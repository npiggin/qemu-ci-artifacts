# qemu-ci-artifacts



## What is it

Collection of open source build images that can be used by QEMU CI

##  Contents

- ppc/corenet64_vmlinux
Linux 6.5
corenet64_smp_defconfig + CONFIG_DEBUG_INFO_NONE=y + CONFIG_MODULES=n
Built with GCC 12.3

- ppc/mpc85xx_vmlinux
Linux 6.5
mpc85xx_defconfig + CONFIG_PPC_QEMU_E500=y +CONFIG_DEBUG_INFO_NONE=y + CONFIG_MODULES=n
Built with GCC 12.3
